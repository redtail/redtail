using System;

namespace RedTail
{
  public abstract class BaseHandler<T1, T2>
    where T1 : BaseRequest
    where T2 : BaseResponse
  {
    public BaseHandler(T1 request, Boolean stopOnError = true)
    {
      this.Name = this.GetType().Name;
      this.Request = request;
      this.Response = Activator.CreateInstance<T2>();
      this.StopOnError = stopOnError;

      this.Response.Reference = this.Request.Reference;
    }

    private readonly String Name;
    private readonly T1 Request;
    private readonly T2 Response;
    private readonly Boolean StopOnError;

    protected abstract void Command(T1 request);
		protected abstract void Done(T2 response);
		protected abstract void Error(T2 response);

    private void Command()
		{
			this.Command(this.Request);
		}

    private void Done()
		{
			this.Response.Code = "OK";
			this.Response.Message = "Success";
			this.Response.Type = ResponseType.Ok;

			this.Done(this.Response);
		}

    private void Error()
		{
			this.Error(this.Response);
		}

    private void Error(BaseException ex)
		{
			this.Response.Code = ex.Code;
			this.Response.Message = ex.Message;
			this.Response.Type = ResponseType.Error;

			this.Error();
		}

		private void Error(Exception ex)
		{
			this.Response.Code = "ER";
			this.Response.Message = ex.Message;
			this.Response.Type = ResponseType.Error;

			this.Error();
		}

    public void Execute()
		{
			try
			{
				this.Command();
				this.Done();
			}
			catch (BaseException ex)
			{
				this.Error(ex);

				if (this.StopOnError) throw ex;
			}
			catch (Exception ex)
			{
				this.Error(ex);

				if (this.StopOnError) throw ex;
			}
		}

    public T2 GetResponse()
		{
			return this.Response;
		}
  }
}
