using System;
using Newtonsoft.Json;

namespace RedTail
{
	public class EnumJsonConverter
		: JsonConverter<Enum>
	{
		public override Enum ReadJson(JsonReader reader, Type objectType, Enum existingValue, Boolean hasExistingValue, JsonSerializer serializer)
		{
			return existingValue;
		}

		public override void WriteJson(JsonWriter writer, Enum value, JsonSerializer serializer)
		{
			if (value == null)
			{
				serializer.Serialize(writer, null);
				return;
			}

			serializer.Serialize(writer, new { Name = value.GetName(), Value = value.GetValue() });
		}
	}
}
