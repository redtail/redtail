using System;

namespace RedTail
{
	public class BaseException
		: Exception
	{
		public String Code { get; set; }

		public BaseException(String code, String message)
			: base(message)
		{
			this.Code = code;
		}

		public BaseException(String code, String message, Exception inner)
			: base(message, inner)
		{
			this.Code = code;
		}
	}
}
