using System;

namespace RedTail
{
  public class BaseResponse
  {
    public String Code { get; set; }
    public String Message { get; set; }
    public String Reference { get; set; }
    public ResponseType Type { get; set; }

    public Boolean Error => this.Type == ResponseType.Error;
    public Boolean Success => this.Type == ResponseType.Ok;
  }
}
