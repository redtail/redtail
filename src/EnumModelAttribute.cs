using System;

namespace RedTail
{
	public class EnumModelAttribute
		: Attribute
	{
		// Declaration

		#region Properties

		public String Name { get; set; } = null;

		public Int32 Position { get; set; } = 0;

		public String Value { get; set; } = null;

		#endregion Properties
	}
}
